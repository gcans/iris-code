import React from 'react';
import { StyleSheet, View } from 'react-native';
import TouchedScreen from './src/TouchedScreen';


export default class App extends React.Component {
  render() {
    return (
      <View style={styles.container}>
        <TouchedScreen />
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'transparent',
  },
});
