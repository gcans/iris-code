import React from 'react';
import {
  Vibration,
  StyleSheet,
  Text,
  View,
} from 'react-native';
import Voice from 'react-native-voice';
import CameraView from '../src/Camera';

class TouchedScreen extends React.Component {
  constructor(props) {
    super(props);
    this.startTouch = this.startTouch.bind(this);
    this.stopTouch = this.stopTouch.bind(this);
    this.state = { isTouched: false };
  }

  startTouch() {
    console.debug("Start Touch on TouchedScreen");
    Vibration.vibrate();
    this.setState({ isTouched:true });
  }

  stopTouch() {
    console.debug("Stop Touch");
  }



  render() {
    if (!this.state.isTouched) {
      return (
      <View style={styles.container}
        onResponderGrant = { () => this.startTouch() }
        onResponderRelease = { () => this.stopTouch() }
        onStartShouldSetResponder = { (e) => {return true} }
        >
        <Text style={styles.welcome}>
  TouchedScreen
          </Text>
        </View>
      );
    }else{
      
      return (
        <View style={styles.container}
        onResponderGrant = { () => this.startTouch() }
        onResponderRelease = { () => this.stopTouch() }
        onStartShouldSetResponder = { (e) => {return true} }
        >
        <CameraView/>
        </View>
      );
    }
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: 'transparent',

  },
  welcome: {
    fontSize: 30,
    textAlign: 'center',
    margin: 20,
    color: '#000000'
  }
});

export default TouchedScreen;
