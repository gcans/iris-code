import React, { Component } from 'react';
import {
  StyleSheet,
  Text,
  View,
} from 'react-native';

class VoiceInput extends Component {


render() {
  console.log("Awaiting Vocal input");
  return (
    <View style={styles.container}>
      <Text style={styles.welcome}>
        Voice
      </Text>
    </View>
  );
}

}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: 'transparent',
  },
  welcome: {
    fontSize: 30,
    textAlign: 'center',
    margin: 20,
    color: '#000000'
  }
});

export default VoiceInput;
